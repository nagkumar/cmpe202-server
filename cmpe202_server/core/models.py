from django.db import models

# Create your models here.
from model_utils.models import TimeStampedModel


class HighScore(TimeStampedModel):
    user_name = models.CharField(max_length=255, null=False, blank=False)
    score = models.CharField(max_length=10, null=False, blank=False)

    def __unicode__(self):
        return "%s %s" % (self.user_name, self.score)

    class Meta:
        verbose_name_plural = 'High Scores'
        ordering = ('-score', '-created')
