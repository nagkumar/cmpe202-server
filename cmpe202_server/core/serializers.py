from rest_framework import serializers

__author__ = 'nagkumar'
from .models import HighScore

class HighScoreSerializer(serializers.ModelSerializer):
    class Meta:
        model = HighScore