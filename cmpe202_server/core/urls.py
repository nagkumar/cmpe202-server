from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.conf import settings


urlpatterns = patterns('core.views',
                       url(r'^high_score/$', 'high_score', name='high_score'),
                       url(r'^$', 'home_page')
)
