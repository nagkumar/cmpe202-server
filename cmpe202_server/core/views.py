from django.shortcuts import render, render_to_response

# Create your views here.
from rest_framework import status
from rest_framework.authentication import SessionAuthentication
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.views.decorators.csrf import csrf_exempt
from .models import HighScore
from .serializers import HighScoreSerializer


class UnsafeSessionAuthentication(SessionAuthentication):
    """
        Basic auth class to avoid CSRF error
    """

    def authenticate(self, request):
        http_request = request._request
        user = getattr(http_request, 'user', None)

        if not user or not user.is_active:
            return None

        return (user, None)


@api_view(['GET', 'POST'])
@csrf_exempt
def high_score(request):
    if request.method == 'GET':
        all_scores = HighScore.objects.all()
        serialized = HighScoreSerializer(all_scores, many=True)
        return Response(serialized.data)
    else:
        try:
            user_name = request.DATA.get("user_name")
            score = request.DATA.get("score")
            high_score = HighScore(user_name=user_name, score=score)
            high_score.save()
            serialized = HighScoreSerializer(high_score)
            return Response(serialized.data)
        except Exception as e:
            print e
            return Response(status=status.HTTP_400_BAD_REQUEST)


def home_page(request):
    return render_to_response("home.html")
